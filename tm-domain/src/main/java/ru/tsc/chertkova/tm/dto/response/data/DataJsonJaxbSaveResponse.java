package ru.tsc.chertkova.tm.dto.response.data;

import lombok.NoArgsConstructor;
import ru.tsc.chertkova.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class DataJsonJaxbSaveResponse extends AbstractResponse {
}
