package ru.tsc.chertkova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.project.ProjectListRequest;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractProjectListener;
import ru.tsc.chertkova.tm.model.Project;

import java.util.List;

@Component
public final class ProjectListListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-list";

    @NotNull
    public static final String DESCRIPTION = "Show project list.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectListListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[PROJECT LIST]");
        //System.out.println("[ENTER SORT:]");
        //System.out.println(Arrays.toString(Sort.values()));
        //@Nullable final String sortType = TerminalUtil.nextLine();
        //@Nullable final Sort sort = Sort.toSort(sortType);
        @Nullable final List<Project> projects = projectEndpoint.listProject(
                new ProjectListRequest(getToken())).getProjects();
        renderProjects(projects);
    }

}
