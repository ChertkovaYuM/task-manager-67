package ru.tsc.chertkova.tm.soap.task;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "success",
    "message"
})
@XmlRootElement(name = "taskPingResponse")
public class TaskPingResponse {

    protected boolean success;

    @XmlElement(required = true)
    protected String message;

}
