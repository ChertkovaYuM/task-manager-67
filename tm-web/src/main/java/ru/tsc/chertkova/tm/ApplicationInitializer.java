package ru.tsc.chertkova.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.tsc.chertkova.tm.config.ApplicationConfiguration;
import ru.tsc.chertkova.tm.config.WebApplicationConfiguration;

import javax.servlet.ServletContext;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @NotNull
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{
                ApplicationConfiguration.class
        };
    }

    @NotNull
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{
                WebApplicationConfiguration.class
        };
    }

    @NotNull
    @Override
    protected String[] getServletMappings() {
        return new String[]{
                "/"
        };
    }

    @Override
    protected void registerContextLoaderListener(@NotNull final ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
    }

}
