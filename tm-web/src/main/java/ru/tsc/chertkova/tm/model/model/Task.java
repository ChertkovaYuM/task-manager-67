package ru.tsc.chertkova.tm.model.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.util.DateUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@XmlAccessorType(XmlAccessType.FIELD)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractFieldsModel {

    private static final long serialVersionUID = 1;

    @Nullable
    @ManyToOne
    @JsonIgnore
    private Project project;

    public Task(@NotNull final String name,
                @NotNull final Status status,
                @Nullable final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public Task(@NotNull final String name,
                @NotNull final String description,
                @Nullable final String projectId) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return getId() + " - " + name + " : " +
                description + ", " + status + ", " + DateUtil.toString(dateBegin);
    }

    public String getProjectId() {
        return project.getId();
    }

}
