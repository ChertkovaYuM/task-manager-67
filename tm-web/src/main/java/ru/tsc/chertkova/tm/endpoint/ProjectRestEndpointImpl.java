package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.chertkova.tm.api.endpoint.ProjectEndpoint;
import ru.tsc.chertkova.tm.model.model.Project;
import ru.tsc.chertkova.tm.service.ProjectService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpointImpl implements ProjectEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService
                .findAll()
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@NotNull @PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectService.existsById(id);
    }

    @Override
    @PostMapping("/save")
    public Project save(@NotNull @RequestBody final Project project) {
        return projectService.save(project);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final Project project) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@NotNull @RequestBody final List<Project> projects) {
        projectService.remove(projects);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear();
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        projectService.removeById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

}
