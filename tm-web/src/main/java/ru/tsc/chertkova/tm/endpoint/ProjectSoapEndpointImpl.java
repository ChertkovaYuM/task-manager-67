package ru.tsc.chertkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.chertkova.tm.api.endpoint.soap.IProjectSoapEndpointImpl;
import ru.tsc.chertkova.tm.service.ProjectService;
import ru.tsc.chertkova.tm.soap.project.*;

import java.util.stream.Collectors;

@Endpoint
public class ProjectSoapEndpointImpl implements IProjectSoapEndpointImpl {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "/ProjectSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tsc.ru/chertkova/tm";

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    public ProjectCreateResponse create(
            @NotNull
            @RequestPayload final ProjectCreateRequest request
    ) {
        @NotNull final ProjectCreateResponse response = new ProjectCreateResponse();
        response.setProject(projectService.create(request.getName()));
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(
            @NotNull
            @RequestPayload final ProjectFindAllRequest request
    ) {
        @NotNull final ProjectFindAllResponse response = new ProjectFindAllResponse();
        response.setProject(projectService
                .findAll()
                .stream()
                .collect(Collectors.toList())
        );
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(
            @NotNull
            @RequestPayload final ProjectFindByIdRequest request
    ) {
        @NotNull final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        response.setProject(projectService.findById(request.getId()));
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(
            @NotNull
            @RequestPayload final ProjectExistsByIdRequest request
    ) {
        @NotNull final ProjectExistsByIdResponse response = new ProjectExistsByIdResponse();
        response.setExists(projectService.existsById(request.getId()));
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(
            @NotNull
            @RequestPayload final ProjectSaveRequest request
    ) {
        projectService.save(request.getProject());
        return new ProjectSaveResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(
            @NotNull
            @RequestPayload final ProjectDeleteRequest request
    ) {
        projectService.remove(request.getProject());
        return new ProjectDeleteResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse clear(
            @NotNull
            @RequestPayload final ProjectDeleteAllRequest request
    ) {
        projectService.remove(request.getProject());
        return new ProjectDeleteAllResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(
            @NotNull
            @RequestPayload final ProjectClearRequest request
    ) {
        projectService.clear();
        return new ProjectClearResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(
            @NotNull
            @RequestPayload final ProjectDeleteByIdRequest request
    ) {
        projectService.removeById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(
            @NotNull
            @RequestPayload final ProjectCountRequest request
    ) {
        @NotNull final ProjectCountResponse response = new ProjectCountResponse();
        response.setCount(projectService.count());
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectPingRequest", namespace = NAMESPACE)
    public ProjectPingResponse ping(
            @NotNull
            @RequestPayload final ProjectPingRequest request
    ) {
        @NotNull final ProjectPingResponse response = new ProjectPingResponse();
        response.setSuccess(true);
        return response;
    }

}
