package ru.tsc.chertkova.tm.soap.task;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.chertkova.tm.model.model.Task;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "task"
})
@XmlRootElement(name = "taskSaveRequest")
public class TaskSaveRequest {

    protected Task task;

}
