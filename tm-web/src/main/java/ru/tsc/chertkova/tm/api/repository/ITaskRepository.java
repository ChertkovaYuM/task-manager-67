package ru.tsc.chertkova.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.chertkova.tm.model.model.Task;

public interface ITaskRepository extends JpaRepository<Task, String> {
}
