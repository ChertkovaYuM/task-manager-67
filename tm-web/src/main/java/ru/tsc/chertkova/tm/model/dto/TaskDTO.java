package ru.tsc.chertkova.tm.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.util.DateUtil;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "name")
    private String name = "";

    @NotNull
    @Column(name = "description")
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @NotNull
    @Column(name = "created_dt")
    private Date created = new Date();

    @Nullable
    @Column(name = "started_dt")
    private Date dateBegin;

    @Nullable
    @Column(name = "completed_dt")
    private Date dateEnd;

    public TaskDTO(@NotNull final String name,
                   @NotNull final Status status,
                   @Nullable final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public TaskDTO(@NotNull final String name,
                   @NotNull final String description,
                   @Nullable final String projectId) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return getId() + " - " + name + " : " +
                description + ", " + status + ", " + DateUtil.toString(dateBegin);
    }

}
