package ru.tsc.chertkova.tm.soap.project;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.chertkova.tm.model.model.Project;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "project"
})
@XmlRootElement(name = "projectSaveRequest")
public class ProjectSaveRequest {

    protected Project project;

}
